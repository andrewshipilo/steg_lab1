import sys
import bitarray
import os
from PySide2.QtWidgets import *
from PySide2.QtCore import Slot

# All english symbols that resemble russian ones
ENG = ['A', 'a', 'C', 'c', 'E', 'e', 'H', 'K', 'O', 'o', 'P', 'p', 'T', 'X', 'x']

# All russian symbols
RUS = ['А', 'а', 'С', 'с', 'Е', 'е', 'Н', 'К', 'О', 'о', 'Р', 'р', 'Т', 'Х', 'х']

# Добавить ёмкость контейнера при считывании
# Добавить информацию о том, что фраза была вставлена
# Проверять заполнен ли контейнер

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'StegoLab1'
        self.fileName = 'None'
        self.verticalGroupBox = QGroupBox()
        self.capacityLabel = QLabel('')
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)

        self.layout = QVBoxLayout()
        self.getLayoutButton = QPushButton('GET', self)
        self.getLayoutButton.clicked.connect(self.createGetLayout)
        self.layout.addWidget(self.getLayoutButton)

        self.insertLayoutButton = QPushButton("INSERT", self)
        self.insertLayoutButton.clicked.connect(self.createInsertLayout)
        self.layout.addWidget(self.insertLayoutButton)

        self.verticalGroupBox.setLayout(self.layout)

        self.windowLayout = QVBoxLayout()
        self.windowLayout.addWidget(self.verticalGroupBox)

        self.setLayout(self.windowLayout)
        self.show()

    @Slot()
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getOpenFileName(self, "Choose file to open", "",
                                                       "Text files (*.txt);;Html files (*.html);;"
                                                       "Cpp files (*.cpp);;Word files (*.docx);; All files (*.*)", options=options)
        if self.fileName:
            self.fileNameLabel.setText("File chosen: " + self.fileName)
            # Calculate container capacity

            with open(self.fileName) as f:
                lines = f.readlines()

            capacity = 0

            for line in lines:
                for symbol in line:
                    if symbol in RUS:
                        capacity += 1

            self.capacityLabel.setText('Capacity is ' + str(capacity) + ' bits')
            print('File size is', os.path.getsize(self.fileName))

    @Slot()
    def insertPhrase(self):
        filename = os.path.splitext(os.path.basename(self.fileName))[0]
        extension = os.path.splitext(os.path.basename(self.fileName))[1]

        ba = bitarray.bitarray()
        ba.frombytes(self.textbox.text().encode('utf-8'))
        bits = ba.tolist()

        with open(self.fileName) as f:
            lines = f.readlines()

        current_bit = 0
        bits_added = 0
        found_start = False
        found_end = False

        new_lines = []
        for line in lines:
            new_line = ''
            for symbol in line:
                if symbol in RUS:
                    if current_bit < len(bits) and bits[current_bit]:
                        symbol = ENG[RUS.index(symbol)]
                    elif current_bit >= len(bits) and not found_end:
                        new_line += u"\u2063"
                        found_end = True
                        bits_added = current_bit
                    current_bit += 1
                    if not found_start:
                        new_line += u"\u2063"  # Add invisible character to indicate
                        found_start = True

                new_line += symbol
            new_lines.append(new_line)

        print(str(len(bits)))
        print(str(bits_added))
        bits_left = len(bits) - bits_added

        if bits_left == 0:
            self.confirmLabel.setText('Phrase was fully inserted')
        else:
            new_lines[-1] += u"\u2063"
            bits_left = len(bits) - current_bit
            self.confirmLabel.setText('Phrase partially added, because container is too small, ' + str(bits_left) + ' bits of phrase left')

        with open(filename + '_res' + extension, 'w+') as f:
            f.writelines(new_lines)

    @Slot()
    def getPhrase(self):
        with open(self.fileName) as f:
            lines = f.readlines()

        ba = bitarray.bitarray()

        block_found = False

        for line in lines:
            for symbol in line:
                if symbol == u"\u2063":
                    # We found our block
                    block_found = not block_found
                    print(block_found)

                if block_found:
                    if symbol in ENG:
                        ba.append(1)
                    elif symbol in RUS:
                        ba.append(0)

        table = str.maketrans(dict.fromkeys('\x00'))
        print(ba)
        phrase = ba.tostring().translate(table)
        self.phraseLabel.setText(phrase)

    @Slot()
    def createGetLayout(self):
        QWidget().setLayout(self.layout)
        self.layout = QVBoxLayout()

        self.fileNameLabel = QLabel("File chosen: " + self.fileName)
        self.layout.addWidget(self.fileNameLabel)

        chooseButton = QPushButton('Choose file', self)
        chooseButton.setToolTip('Open file')
        chooseButton.clicked.connect(self.openFileNameDialog)
        self.layout.addWidget(chooseButton)

        self.layout.addWidget(self.capacityLabel)

        getButton = QPushButton('Get', self)
        getButton.clicked.connect(self.getPhrase)
        self.layout.addWidget(getButton)

        self.phraseLabel = QLabel('')
        self.layout.addWidget(self.phraseLabel)


        self.verticalGroupBox.setLayout(self.layout)


    @Slot()
    def createInsertLayout(self):
        QWidget().setLayout(self.layout)
        self.layout = QVBoxLayout()

        label1 = QLabel("Enter phrase to insert")
        self.layout.addWidget(label1)

        self.textbox = QLineEdit(self)
        self.layout.addWidget(self.textbox)

        self.fileNameLabel = QLabel("File chosen: " + self.fileName)
        self.layout.addWidget(self.fileNameLabel)

        chooseButton = QPushButton('Choose file', self)
        chooseButton.setToolTip('Open file')
        chooseButton.clicked.connect(self.openFileNameDialog)
        self.layout.addWidget(chooseButton)

        self.layout.addWidget(self.capacityLabel)

        insertButton = QPushButton('INSERT', self)
        insertButton.clicked.connect(self.insertPhrase)
        self.layout.addWidget(insertButton)

        self.confirmLabel = QLabel('')
        self.layout.addWidget(self.confirmLabel)

        self.verticalGroupBox.setLayout(self.layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
