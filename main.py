import bitarray
import textract

input_file = 'test' #'test.html'
output_file = 'test_res' #'test_res.html'

ba = bitarray.bitarray()
ba.frombytes(TEXT.encode('utf-8'))
bits = ba.tolist()
print(bits)

lines = textract.process(input_file).decode("utf-8")
print(lines)

current_bit = 0

new_lines = []
for line in lines:
    new_line = ''
    for symbol in line:
        if symbol in RUS:
            if current_bit < len(bits) and bits[current_bit]:
                symbol = ENG[RUS.index(symbol)]
                print(symbol)
            current_bit += 1

        if current_bit == 1:
            new_line += u"\u2063"

        new_line += symbol
    new_lines.append(new_line)  # Add invisible character to indicate


with open(output_file, 'w+') as f:
    f.writelines(new_lines)

# Extract phrase
with open(output_file) as f:
    lines = f.readlines()

ba = bitarray.bitarray()

block_found = False

for line in lines:
    for symbol in line:
        if symbol == u"\u2063":
            # We found our block
            block_found = True

        if block_found:
            if symbol in ENG:
                ba.append(1)
            elif symbol in RUS:
                ba.append(0)

table = str.maketrans(dict.fromkeys('\x00'))
phrase = ba.tostring().translate(table)

print(phrase)
